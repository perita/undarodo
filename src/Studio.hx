import hxd.Res;

class Studio extends Stage {
  public function new(font:h2d.Font, scene:h2d.Scene) {
    super(font, scene);

    final background = new AtlasBitmap("background_studio", root);

    t800 = new Actor("t800", background);
    t800.setPosition(Std.int(375 + 245 / 2), 4 + 160);
    t800.playAnim("lay");

    pomme = new Actor("pomme", background);
    pomme.setPosition(200, 355);
    pomme.playAnim("paint");

    final throwing = new AtlasAnim("throw_throw", 10, background);
    throwing.flip();
    throwing.visible = false;
    throwing.loop = false;

    addHotspot(380, 50, 245, 160, "Terminator");
    whenLook("I wonder if he’ll sing “My Heart\nWill Go On” next.");

    addHotspot(1100, 0, 250, 375, "“Under\nConstruction”");
    whenLookCb(() -> {
	say(raimunda, "What is that?");
	oneShot(() -> if (rodo.flipped) rodo.flip());
	walk(rodo, 880);
	say(rodo, "Well,  SOMEONE  did not\nplan correctly for the jam.");
	say(rodo, "So, had to put one of these\n“under construction” web banners.");
	oneShot(() -> if (!raimunda.flipped) raimunda.flip());
	say(raimunda, "A web banner in 1929?");
	wait(.5);
	say(rodo, "Things are worse than I thought.");
	oneShot(() -> rodo.flip());
        walk(rodo, 680);
      });
    addHotspot(Std.int(pomme.x -230 / 2), Std.int(pomme.y-220), 230, 220, "Pomme");
    whenTalk(() -> {
	walk(raimunda, Std.int(pomme.x + 180));
	callback((dt) -> {
	    root.x += 100 * dt;
	    if (root.x >= 0) {
	      root.x = 0;
	      pending.shift();
	    }
	  });
	oneShot(() -> {
	    pomme.playAnim("stand");
	    if (!raimunda.flipped) raimunda.flip();
	    if (!rodo.flipped) rodo.flip();
	  });

	if (triggered("suggestedFrench") > 0) {
	  final blind = new h2d.Bitmap(h2d.Tile.fromColor(0xffffff), scene);
	  blind.width = scene.width;
	  blind.height = scene.height;
	  blind.alpha = 0;

	  final left_lightning = new AtlasBitmap("background_lightning_left", scene);
	  left_lightning.setPosition(138, -75);
	  left_lightning.visible = false;
	  final right_lightning = new AtlasBitmap("background_lightning_right", scene);
	  right_lightning.setPosition(523, -75);
	  right_lightning.visible = false;

	  var lightning_time = 1.0;

	  walk(rodo, 680);
	  wait(.25);
	  say(pomme, "Ah, quelle belle raisin!");
	  wait(.25);
	  wait(1);
	  oneShot(() -> {
	      pomme.playAnim("fright");
	      raimunda.x = pomme.x + 150;
	      raimunda.playAnim("seethe");
	      Res.bgm2.stop();
	      Res.bgm3.play(true);
	    });
	  say(raimunda, "What did you call me?");
	  say(raimunda, "A “raisin”?!");
	  say(raimunda, "That’s   IT!!");
	  oneShot(() -> {
	      Res.sfx_roar.play();
	      raimunda.playAnim("roar");
	      t800.y += 60;
	      t800.playAnim("stand");
	    });
	  wait(1);
	  say(pomme, "No, Miss, in French...");
	  oneShot(() -> {
	      raimunda.visible = false;
	      pomme.visible = false;
	      throwing.setPosition(raimunda.x - 70, raimunda.y);
	      throwing.playAnim("throw_throw");
	      throwing.visible = true;
	    });
	  wait(0.5);
	  oneShot(() -> Res.sfx_window.play());
	  callback((dt) -> {
	      if (throwing.currentFrame == throwing.frames.length) {
		Res.bgm3.stop();
		pomme.x = 50;
		pending.shift();
	      }
	    });
	  wait(1);
	  say(pomme, "Mon Dieu!");
	  say(pomme, "Le pain!");
	  oneShot(() -> {
	      Res.bgm2.play(true);
	      raimunda.visible = true;
	      raimunda.playAnim("seethe");
	      throwing.remove();
	      final broken = new AtlasBitmap("background_broken", background);
	      broken.x = 20;
	    });
	  say(raimunda, "Who is dried and wrinkled,\nnow, huh, “earth apple”?");
	  oneShot(() -> {
	      t800.playAnim("fright");
	      raimunda.flip();
	      raimunda.playAnim("point");
	    });
	  say(raimunda, "And you!");
	  say(raimunda, "Return to your time!");
	  say(raimunda, "NOW!");
	  oneShot(() -> Res.sfx_thunder.play());
	  callback((dt) -> {
	      lightning_time -= dt;
	      if (lightning_time <= 0) {
		pending.shift();
	      } else {
		left_lightning.visible = Math.random() > 0.5;
		right_lightning.visible = Math.random() > 0.5;
		blind.alpha = Math.random() > 0.5 ? 255 : 0;
	      }
	    });
	  oneShot(() -> {
	      blind.alpha = 0;
	      left_lightning.remove();
	      right_lightning.remove();
	      t800.remove();
	    });
	  wait(.5);
	  oneShot(() -> {
	      raimunda.playAnim("seethe");
	    });
	  wait(.5);
	  walk(rodo, 580);
	  wait(1);
	  say(rodo, "I  THINK  your pitta is out of\nbalance, isn’t it?");
	  say(raimunda, "Shuddap!!");
	  say(raimunda, "Let’s go back to the office.");
	  say(raimunda, "I just want my mac and cheese!!");
	  say(rodo, "Okay, okay.");
	  wait(.5);
	  oneShot(() -> {
	      Res.sfx_epoch.play();

	      callback((dt) -> {
		  blind.alpha += dt;
		  if (blind.alpha > 1.5) {
		    Res.bgm2.stop();
		    pending.shift();
		    Main.ME.setStage((font, scene) -> new Outro(font, scene));
		  }
		});
	    });
	} else {
	  switch trigger("talkToPomme") {
	      case 1:
		say(raimunda, "Hello there!");
		wait(1.5);
		say(raimunda, "Hello...?");
		say(pomme, "Pardon me for staring, Miss.");
		say(pomme, "But, you’re breathtakingly beautiful.");
		say(raimunda, "Thank you, but...");
		say(pomme, "May I paint you next?");
		say(raimunda, "I’m sorry?");
		say(pomme, "I want to capture your beauty\nby painting you.");
		say(raimunda, "I’m flattered, but...");
		say(pomme, "Naked, of course.");
		wait(1);
		say(raimunda, "Nooo, thanks.");
		case 2:
		  say(pomme, "Have you changed your mind, Miss?");
		say(raimunda, "No, no, no.");
		default:
		  say(pomme, "Are you ready to be my next model?");
		  say(pomme, "And maybe more....");
		say(raimunda, "Stop it!");
	    }
		say(pomme, "What a pity.");
	  oneShot(() -> {
	      pomme.playAnim("paint");
	    });
	  callback((dt) -> {
	      final target = Std.int(-raimunda.x + scene.width / 2);
	      root.x -= 100 * dt;
	      if (root.x <= target) {
		root.x = target;
		pending.shift();
	      }
	    });
	}
      });

    rodo = new Actor("rodo", background);
    rodo.setPosition(440 + 667, 355);
    rodo.flip();
    rodo.playAnim("stand");

    addHotspot(Std.int(680 - 220 / 2), 355 - 260, 220, 260, "Bacon");
    whenTalk(() -> {
	walk(raimunda, 680 - 140);
	oneShot(() -> if (raimunda.flipped) raimunda.flip());
	if (triggered("suggestedFrench") > 0) {
	  say(rodo, "I don’t know what else I can do.");
	  say(raimunda, "Well, thanks.");
	} else if (triggered("talkToPomme") > 2) {
	  say(raimunda, "Have you heard that sleazeball?");
	  say(raimunda, "He’s a pig!");
	  say(rodo, "Hey!");
	  say(raimunda, "Sorry.");
	  wait(.5);
	  say(raimunda, "Could you do me a solid\nand talk to him?");
	  say(rodo, "Me? Talk to people?");
	  say(raimunda, "C’mon! It’s easy!");
	  say(rodo, "As easy as when “we” recalibrated\nthe carbon nanotube resonator?");
	  wait(.5);
	  say(raimunda, "Easier, even.");
	  say(rodo, "Humph.");
	  wait(.5);
	  say(rodo, "Okay, I’ll try.");
	  callback((dt) -> {
	      root.x += 100 * dt;
	      if (root.x >= 0) {
		root.x = 0;
		pending.shift();
		if (!raimunda.flipped) raimunda.flip();
		if (!rodo.flipped) rodo.flip();
	      }
	    });
	  walk(rodo, Std.int(pomme.x + 180));
	  oneShot(() -> {
	      pomme.playAnim("stand");
	    });
	  say(rodo, "He... Hello.");
	  say(pomme, "Who are you?");
	  say(rodo, "I’m Gordon...");
	  say(pomme, "Are  YOU  with  HER?");
	  say(rodo, "We’re colleagues, if that’s what you mean.");
	  say(pomme, "She is pretty, yes?");
	  say(rodo, "I... I guess.");
	  say(pomme, "But I don’t think she likes me very much.");
	  say(pomme, "I don’t understand why.");
	  say(rodo, "I didn’t think French-speaking fruits\nhad  THAT  problem.");
	  say(pomme, "French! Of course!");
	  say(pomme, "That’s how I’ll woo her.");
	  say(pomme, "Thanks, little pig.");
	  say(pomme, "Now, let me work.");
	  oneShot(() -> {
	      pomme.playAnim("paint");
	    });
	  wait(0.5);
	  oneShot(() -> {
		rodo.flip();
	    });
	  callback((dt) -> {
	      final target = Std.int(-raimunda.x + scene.width / 2);
	      root.x -= 100 * dt;
	      if (root.x <= target) {
		root.x = target;
		pending.shift();
	      }
	    });
	  walk(rodo, 680);
	  oneShot(() -> {
	      raimunda.flip();
	      rodo.flip();
	    });
	  say(rodo, "I did what I could.");
	  say(raimunda, "Argh. My patience is wearing thin.");
	  trigger("suggestedFrench");
	} else {
	  switch (trigger("talkToRodo")) {
	  case 1:
	    say(raimunda, "So, what should we do?");
	    say(rodo, "I’m not sure.");
	    say(rodo, "We have to convince the\nartist to drop the painting.");
	  default:
	    say(raimunda, "How do we convince the artist?");
	    say(rodo, "I don’t really know.");
	    say(rodo, "It’s not me who’s good with people.");
	    say(raimunda, "Right.");
	  }
	}
      });

    raimunda = ego = new Actor("raimunda", background);
    raimunda.setPosition(180 + 667, 355);
    raimunda.playAnim("stand");

    oneShot(() -> Res.bgm2.play(true));

    maxOffset = 667;
    root.x = -667;

    wait(1);
    say(rodo, "Ugh!");
    say(rodo, "I always get queasy when time traveling.");
    say(raimunda, "That’s because you always have\nyour doshas unbalanced.");
    say(rodo, "I tend to favour kapha, yes.");
    oneShot(() -> {
	rodo.playAnim("bellygrab");
	rodo.loop = false;
	  });
    callback((_) -> if (rodo.currentFrame == rodo.frames.length) {
	rodo.playAnim("stand");
	rodo.loop = true;
	pending.shift();
      });
    wait(.5);
    say(raimunda, "Exactly.");
    wait(1);
    say(raimunda, "Are we at the right time?");
    say(rodo, "I believe so.");
    say(rodo, "Look.");
    oneShot(() -> raimunda.flip());
    wait(0.5);
    callback((dt) -> {
        final target = 0;
	root.x += 200 * dt;
	if (root.x >= target) {
	  root.x = target;
	  pending.shift();
	}
      });
    wait(0.5);
    say(t800, "Paint me like one of your Sarah Connors.");
    callback((dt) -> {
        final target = -667;
	root.x -= 200 * dt;
	if (root.x <= target) {
	  root.x = target;
	  pending.shift();
	}
      });
    oneShot(() -> raimunda.flip());
    say(raimunda, "Wow!");
    say(rodo, "Indeed, wow.");
    say(rodo, "Let’s fix this mess.");

    oneShot(() -> raimunda.flip());
    walk(rodo, 680);
    callback((dt) -> {
        final target = Std.int(-raimunda.x + scene.width / 2);
	root.x += 100 * dt;
	if (root.x >= target) {
	  root.x = target;
	  pending.shift();
	}
      });
    say(raimunda, "C’mon, that lunch is waiting.");
  }

  function trigger(name:String): Int {
    var v = triggered(name);
    v += 1;
    triggers[name] = v;
    return v;
  }

  function triggered(name:String): Int {
    var v = triggers[name];
    return v == null ? 0 : v;
  }

  final pomme:Actor;
  final t800:Actor;
  final raimunda:Actor;
  final rodo:Actor;
  final triggers:Map<String,Int> = [];
}