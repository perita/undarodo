import h2d.Bitmap;
import h2d.Font;
import h2d.Scene;
import h2d.Object;
import h2d.Text;
import h2d.Tile;
import hxd.Cursor;
import hxd.System;


enum Action{
  Turn(who:Actor);
  Walk(who:Actor, where:Int, onArrive:Void->Void);
  Wait(when:Float);
  Say(who:Actor, what:String);
  Anim(who:Actor, anim:String);
  Callback(cb:Float->Void);
}


class Hotspot extends h2d.Interactive {
  public var description:()->Void;
  public var onPick:()->Void;
  public var onTalk:()->Void;
  public var onUseObject:()->Void;
}


class Stage {
  public function new(font:Font, scene:Scene) {
    this.font = font;
    this.scene = scene;
    maxOffset = 0;
    root = new Object(scene);
    System.setCursor = (cur:Cursor) -> {
      if (cur != Hide && pickedObject != null) {
	if (pending.length == 0) {
	  System.setNativeCursor(pickedObject);
	}
      } else if (cur == Default) {
	if (pending.length == 0) {
	  System.setNativeCursor(Main.ME.defaultCursor);
	}
      } else {
	System.setNativeCursor(cur);
      }
    }

  }

  public function onMouseDown(event:hxd.Event) {
    if (speech != null) {
      speech.visible = false;
    } else if (pending.length < 2) {
      walk(ego, Std.int(event.relX - root.x), true);
    }
  }

  public function update(dt:Float) {

    if (pending.length == 0) return;
    switch(pending[0]) {
    case Say(who, what):
      if (speech == null) {
	speech = new Speech(font, who.x, who.y - 220, what, who.speechColor, scene.width - root.x, root);
      } else if (!speech.visible) {
	speech.remove();
	speech = null;
	pending.shift();
      }
    case Anim(who, anim):
      who.playAnim(anim);
      pending.shift();
    case Turn(who):
      who.flip();
      pending.shift();
    case Walk(who, where, onArrive):
      if (who.animName != "walk") {
	if (Math.abs(where - who.x) < 10) {
	  pending.shift();
	  if (onArrive != null) {
	    onArrive();
	  }
	  return;
	}
	if ((who.flipped && where > who.x) || (!who.flipped && where < who.x)) who.flip();
	who.playAnim("walk");
      }
      if ((who.flipped && who.x < where) || (!who.flipped && who.x > where)) {
	who.playAnim("stand");
	pending.shift();
	if (onArrive != null) {
	  onArrive();
	}
      } else {
	who.x += 150 * dt * (who.flipped ? -1 : 1);
	if (who == ego) {
	  root.x = Std.int(Math.min(0, Math.max(-maxOffset, -who.x + scene.width / 2)));
	}
      }
    case Wait(when):
      if (when > dt) {
	pending[0] = Wait(when - dt);
      } else {
	pending.shift();
      }
    case Callback(cb):
      cb(dt);
    }

    if (pending.length == 0) {
      System.setCursor(Default);
      for(hs in hotspots) {
	hs.visible = true;
      }
    }
  }

  overload extern inline function addHotspot(x:Int, y:Int, w:Int, h:Int, name:String, ?parent) {
    final hs = new Hotspot(w, h, root);
    hs.setPosition(x, y);
    hs.name = name;
    hs.onOver = (_) -> showName(hs);
    hs.onOut = (_) -> hideName();
    hs.onClick = (_) -> {
      walkToHotspot(ego, hs, () -> triggerHotspot(hs));
    }
    hotspots.push(hs);
    lastHotspot = hs;
  }

  overload extern inline function addHotspot(obj:Bitmap, name:String) {
    addHotspot(Std.int(obj.x), Std.int(obj.y), obj.tile.iwidth, obj.tile.iheight, name);
  }

  function whenLook(description:String, hs = null) {
    whenLookCb(()->say(ego, description), hs);
  }

  function whenLookCb(description:()->Void, hs = null) {
    if (hs == null) {
      hs = lastHotspot;
    }
    hs.cursor = Main.ME.eyeCursor;
    hs.description = description;
  }

  function whenTalk(onTalk:Void->Void, hs = null) {
    if (hs == null) {
      hs = lastHotspot;
    }
    hs.cursor = Main.ME.talkCursor;
    hs.onTalk = onTalk;
  }

  function whenPick(onPick:Void->Void, hs = null) {
    if (hs == null) {
      hs = lastHotspot;
    }
    hs.cursor = Main.ME.handCursor;
    hs.onPick = onPick;
  }

  function whenUseObject(onUseObject:()->Void, hs = null) {
    if (hs == null) {
      hs = lastHotspot;
    }
    hs.onUseObject = onUseObject;
  }
  function triggerHotspot(hs:Hotspot) {
    if (pickedObject != null) {
      if (hs.onUseObject == null) {
	final options = [
			 "This won’t work.",
			 "These two don’t go toghether.",
			 "I do not know how to do that.",
			 ];
	say(ego, options[Std.int(Math.random() * options.length)]);
      } else {
	hs.onUseObject();
      }
    } else if (hs.description != null) {
      hs.description();
    } else if (hs.onPick != null) {
      hideName();
      hs.remove();
      hotspots.remove(hs);
      hs.onPick();
    } else if (hs.onTalk != null) {
      hideName();
      hs.onTalk();
    }
  }

  function callback(cb:Float->Void) {
    addAction(Callback(cb));
  }

  function oneShot(cb:Void->Void) {
    callback((_) -> {
	cb();
	pending.shift();
      });
  }

  function say(who:Actor, what:String) {
    addAction(Say(who, what));
  }

  function anim(who:Actor, anim:String) {
    addAction(Anim(who, anim));
  }

  function turn(who:Actor) {
    addAction(Turn(who));
  }

  function walk(who:Actor, where:Int, replace = false, onArrive = null) {
    if (replace && pending.length > 0) {
      switch pending[0] {
	  case Walk(pwho, _) if (pwho == who):
	    if ((who.flipped && where > who.x) || (!who.flipped && where < who.x)) who.flip();
	    pending[0] = Walk(who, where, onArrive);
	    return;
	    default:
	}
    }
    pending.push(Walk(who, where, onArrive));
  }

  function wait(when:Float) {
    addAction(Wait(when));
  }

  function addAction(action:Action) {
    pending.push(action);
    System.setCursor(Hide);
    for(hs in hotspots) {
      hs.visible = false;
    }
  }

  function walkToHotspot(who:Actor, hs:Hotspot, onArrive:Void->Void) {
    final x = who.x;
    final left = hs.x - 80;
    final right = hs.x + hs.width + 80;
    final target = Std.int(x < left ? left : x > right ? right : x);
    if ((who.flipped && hs.x > who.x) || (!who.flipped && hs.x < who.x)) who.flip();
    walk(who, target, true, onArrive);
  }

  function hideName() {
    if (name != null) {
      name.remove();
      name = null;
    }
  }

  function showName(hs:Hotspot) {
    hideName();
    final text = new Text(font);
    text.textColor = 0x573216;
    text.text = hs.name;
    text.setPosition(10, 0);
    text.smooth = true;

    final background = Tile.fromColor(0xffffff, Std.int(text.textWidth) + 20, Std.int(text.textHeight) + 10, .75);
    name = new Bitmap(background, root);
    name.x = Std.int(Math.max(0, hs.x + hs.width / 2 - background.width / 2));
    name.y = Std.int(Math.max(0, hs.y - background.height));
    name.addChild(text);
  }

  final root:Object;
  final scene:Scene;
  final font:Font;
  final pending:Array<Action> = [];
  final hotspots:Array<Hotspot> = [];

  var ego:Actor;
  var speech:Speech;
  var lastHotspot:Hotspot;
  var name:Bitmap;
  var pickedObject:Cursor;
  var maxOffset:Int;
}