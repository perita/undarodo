import h2d.Text;
import hxd.Res;

class Speech extends h2d.Object {
  public function new(font:h2d.Font, x:Float, y:Float, line:String, backgroundColor: Int, sceneWidth:Float, parent:h2d.Object) {
    super(parent);

    final bubble = new h2d.ScaleGrid(Res.tiles.get("speech_bubble"), 32, 32, 32, 32, this);
    final tail = new h2d.Bitmap(Res.tiles.get("speech_tail"), this);

    final text = new Text(font, bubble);
    text.setPosition(32, 0);
    text.textColor = 0x573216;
    text.letterSpacing = 0;
    text.smooth = true;
    text.textAlign = Align.Center;
    text.text = line;

    bubble.color.setColor(backgroundColor);
    tail.color.setColor(backgroundColor);
    bubble.width = text.textWidth + 64;
    text.x = bubble.width / 2;
    bubble.height = Math.max(64, text.textHeight + 12);
    text.y = bubble.height / 2 - text.textHeight / 2 - 6;
    bubble.setPosition(Math.max(15, Math.min(x - bubble.width / 2, sceneWidth - bubble.width)), Math.max(0, y - bubble.height - tail.tile.height));
    tail.setPosition(x - tail.tile.width / 2, bubble.y + bubble.height);
  }
}