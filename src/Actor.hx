class Actor extends AtlasAnim {
  public final speechColor:Int;
  
  public function new (name:String, ?parent:h2d.Object) {
    super(null, 5, parent);
    this.name = name;
    this.speechColor = switch(name) {
    case "rodo": 0xff00aadf;
    case "raimunda": 0xff00d455;
    case "pomme": 0xffffd42a;
    case "t800": 0xffbc5fd3;
    default: 0xff000000;
    }
  }

  override function anim(anim:String) {
    return super.anim(name + "_" + anim);
  }
}