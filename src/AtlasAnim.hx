import h2d.Flow;

class AtlasAnim extends h2d.Anim {
  public function new(name:String, speed:Float, ?parent:h2d.Object) {
    super([], speed, parent);
    this.animName = name;
    replay();
    Main.ME.onReloadAtlas(replay);
  }

  function replay() {
    if (animName == null) {
      return;
    }
    playAnim(animName, currentFrame);
  }

  public function playAnim(name:String, atFrame = 0.0) {
    animName = name;
    final frames = anim(animName);
    play(frames, Math.min(frames == null ? 0 : frames.length, atFrame));
    if (flipped) {
      flipFrames();
    }
  }

  function anim(name:String) {
    return hxd.Res.tiles.getAnim(name, FlowAlign.Middle, FlowAlign.Bottom);
  }

  public function flip() {
    flipped = !flipped;
    flipFrames();
  }

  function flipFrames() {
    for (frame in frames) {
      if (frame == null) {
	continue;
      }
      frame.flipX();
    }
  }

  public var flipped = false;
  public var animName:String;
}