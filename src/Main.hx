import h2d.Font;
import h2d.Scene;

import h2d.Tile;
import hxd.Cursor;
import hxd.Res;

class Main extends hxd.App {
  public static var ME: Main;

  public static function main() {
    new Main();
  }

  public function new() {
    ME = this;
    super();
  }

  override function loadAssets(onLoaded: ()->Void) {
#if (hl && debug)
    Res.initLocal();
#else
    Res.initEmbed();
#end
    onLoaded();
  }

  override function init() {
    Res.tiles.watch(reloadAtlas);
    s2d.scaleMode = LetterBox(667, 375, true, Center, Center);
    s2d.camera.clipViewport = true;

    font = Res.AmaticSC.toSdfFont(42, SDFChannel.MultiChannel, 0.5, 4 / 24);

    eyeCursor = loadCursor("eye");
    handCursor = loadCursor("hand");
    talkCursor = loadCursor("talk");
    defaultCursor = loadCursor("crosshairs");

    s2d.addEventListener((event) -> {
	switch event.kind {
	    case EPush:
	      onMouseDown(event);
	      default:
	  }
      });

    setStage((font, scene) -> new Intro(font, scene));
    //setStage((font, scene) -> new Studio(font, scene));
    //setStage((font, scene) -> new Outro(font, scene));
  }

  public function setStage(build:(Font, Scene)->Stage) {
    s2d.removeChildren();
    stage = build(font, s2d);
  }

  function reloadAtlas() {
    @:privateAccess Res.tiles.contents = null;
    for (callback in reloadAtlasCallbacks) {
      callback();
    }
  }

  public function onReloadAtlas(callback:()->Void) {
    reloadAtlasCallbacks.push(callback);
  }

  function loadCursor(name:String): Cursor {
    final tile = Res.tiles.get("cursor_" + name);
    return createCursor(tile);
  }

  public static function createCursor(tile:Tile): Cursor {
    var bitmap = new hxd.BitmapData(tile.iwidth, tile.iheight);
    bitmap.setPixels(tile.getTexture().capturePixels(h2d.col.IBounds.fromValues(tile.ix, tile.iy, tile.iwidth, tile.iheight)));
    return Custom(new CustomCursor([bitmap], 0, Std.int(tile.iwidth / 2), Std.int(tile.iheight / 2)));
  }

  function onMouseDown(event:hxd.Event) {
    if (stage != null) stage.onMouseDown(event);
  }

  override function update(dt:Float) {
    if (stage != null) stage.update(dt);
  }

  public var eyeCursor:Cursor;
  public var handCursor:Cursor;
  public var talkCursor:Cursor;
  public var defaultCursor:Cursor;

  final reloadAtlasCallbacks:Array<()->Void> = new Array();
  var font:Font;
  var stage:Stage;
}
