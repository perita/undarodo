import h2d.Bitmap;
import h2d.Flow;
import h2d.Tile;
import hxd.Res;


class Intro extends Stage {
  public function new(font:h2d.Font, scene:h2d.Scene) {
    super(font, scene);

    final background = new AtlasBitmap("background_kitchen", root);

    addHotspot(33, 98, 52, 87, "Coffee machine");
    whenLook("Just a regular coffee machine.");

    final lunchbox = new AtlasBitmap("background_lunchbox", background);
    lunchbox.setPosition(117, 142);
    addHotspot(lunchbox, "Lunch box");
    whenPick(() -> {
	lunchbox.remove();
	pickedObject = Main.createCursor(lunchbox.tile);
	say(raimunda, "Umm! Mac and cheese!");
      });

    addHotspot(198, 34, 111, 53, "Mugs");
    whenLook("Commemorative mugs for\n“best investigators”.");

    final microwave = new Actor("microwave", background);
    microwave.speed = 20;
    microwave.setPosition(Std.int(198 + 110 / 2), Std.int(117 + 69));
    microwave.playAnim("off");

    final explosion = new AtlasAnim("microwave_explode", 10, microwave);
    explosion.setPosition(0, 50);
    explosion.visible = false;

    addHotspot(198, 117, 110, 69, "Microwave");
    final microHS = lastHotspot;
    whenLook("Is that a new microwave?");
    whenUseObject(() -> {
	pickedObject = null;
	microHS.name = "Delta Prochronism Implanter";
	whenLook("Better not touch that thing again.", microHS);

	final blind = new Bitmap(Tile.fromColor(0xffffff), background);
	blind.width = scene.width;
	blind.height = scene.height;
	blind.alpha = 0;

	walk(raimunda, 140);
	turn(raimunda);
	oneShot(() -> {
	    microwave.playAnim("on");
	    Res.sfx_microwave.play(true);
	  });
	wait(.5);
	say(raimunda, "Cool!");
	wait(.5);
	oneShot(() -> {
	    Res.bgm0.stop();
	    Res.sfx_microwave.stop();
	    Res.sfx_warning.play(true);
	    explosion.visible = true;
	  });
	anim(raimunda, "wary");
	say(raimunda, "Uh-oh....");
	oneShot(() -> {
	    Res.sfx_warning.stop();
	    Res.sfx_explosion.play();
	  });
	callback((dt) -> {
	    blind.alpha += dt;
	    if (blind.alpha > 1.5) {
	      explosion.remove();
	      microwave.playAnim("off");
	      blind.remove();
	      pending.shift();
	    }
	  });
	wait(1);
	anim(raimunda, "stand");
	oneShot(() -> Res.bgm1.play(true));
	say(raimunda, "BACON!");
	say(raimunda, "Could you come here, please?");
	walk(rodo, 540);
	say(rodo, "Yes, Grapes?");
	say(raimunda, "There’s something wrong with the\nmicrowave you’ve brought.");
	say(rodo, "What microwave?");
	walk(rodo, 350);
	say(rodo, "Ummm....");
	say(rodo, "That’s not a microwave!");
	say(raimunda, "What is it, then?");
	say(rodo, "Oooh, shit!");
	walk(rodo, 780);
	wait(.5);
	say(rodo, "Shit! I knew it!");
	say(raimunda, "What?");
	walk(rodo, 540);
	say(rodo, "It's a Delta Prochronism Implanter.");
	say(raimunda, "A what?");
	say(rodo, "A device to fuck up a world’s timeline.");
	say(rodo, "Look.");
	callback(showNewspaper);
	wait(2);
	say(raimunda, "Is that a... Terminator?");
	say(rodo, "Yes.");
	say(rodo, "I mean, no: it’s a  DRAWING  of a T-800.");
	say(rodo, "Saying that it’s a Terminator\nwould be lying because...");
	say(raimunda, "Bacon!");
	say(rodo, "Yes, yes.");
	say(rodo, "This is the Implanter’s fault.");
	say(rodo, "Is used by the Kitt’n’rugs to “play pranks”.");
	callback(hideNewspaper);
	say(raimunda, "The feline humanoids?");
	say(raimunda, "But, why?");
	say(rodo, "Because they are assholes!");
	wait(.5);
	say(raimunda, "So, what we should do?");
	say(rodo, "Let’s use the Epoch to go back\nand fix the timeline up.");
	say(raimunda, "I’m on it!");

	oneShot(() -> {
	    var talked = false;
	    addHotspot(Std.int(540 - 220 / 2), 355 - 260, 220, 260, "Bacon");
	    whenTalk(() -> {
		if (talked) {
		  final blind = new Bitmap(Tile.fromColor(0xffffff), background);
		  blind.width = scene.width;
		  blind.height = scene.height;
		  blind.alpha = 0;

		  say(raimunda, "Am I supposed to know what\nthe Epoch looks like?");
		  say(rodo, "No. Sorry.");
		  walk(rodo, 780);
		  wait(.5);
		  walk(rodo, 540);
		  say(rodo, "Ready?");
		  oneShot(() -> Res.sfx_epoch.play());
		  callback((dt) -> {
		      blind.alpha += dt;
		      if (blind.alpha > 1.5) {
			Res.bgm1.stop();
			pending.shift();
			Main.ME.setStage((font, scene) -> new Studio(font, scene));
		      }
		    });
		} else {
		  talked = true;
		  say(raimunda, "So....  The Epoch, right?");
		  say(rodo, "Yes. The one we got from Chronos.");
		  say(raimunda, "Right....");
		}
	      });
	  });
      });

    addHotspot(449, 46, 217, 160, "Board");
    whenLook("Documents of the dimensions\nwe’ve travelled to.");

    rodo = new Actor("rodo", background);
    rodo.setPosition(780, 355);
    rodo.flip();
    rodo.playAnim("stand");

    raimunda = new Actor("raimunda", background);
    raimunda.setPosition(780, 355);
    raimunda.flip();
    raimunda.playAnim("stand");
    ego = raimunda;

    oneShot(() -> Res.bgm0.play(true));
    walk(raimunda, 480);
    say(raimunda, "Time to fix me some mac and cheese.");
  }

  function showNewspaper(dt:Float) {
    if (newspaper == null) {
      Res.sfx_newspaper.play();
      newspaperBg = new Bitmap(Tile.fromColor(0x504c4c), root);
      newspaperBg.width = scene.width;
      newspaperBg.height = scene.height;
      newspaper = new AtlasBitmap("background_newspaper", newspaperBg, FlowAlign.Middle, FlowAlign.Middle);
      newspaper.setPosition(Std.int(scene.width / 2), Std.int(scene.height / 2));
      newspaper.setScale(.1);
    } else {
      newspaper.rotate(20 * dt);
      newspaper.setScale(newspaper.scaleX + dt);
      if (newspaper.scaleX >= 1) {
	newspaper.setScale(1);
	newspaper.rotation = 0;
	pending.shift();
      }
    }
  }

  function hideNewspaper(dt:Float) {
    newspaperBg.alpha -= dt;
    if (newspaperBg.alpha <= 0) {
      newspaper.remove();
      newspaper = null;
      newspaperBg.remove();
      newspaperBg = null;
      pending.shift();
    }
  }

  final rodo:Actor;
  final raimunda:Actor;
  var newspaper:Bitmap;
  var newspaperBg:Bitmap;
}
