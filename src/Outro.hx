import hxd.Res;

class Outro extends Stage {
  public function new(font:h2d.Font, scene:h2d.Scene) {
    super(font, scene);

    final background = new AtlasBitmap("background_kitchen", root);

    final rodo = new Actor("rodo", background);
    rodo.setPosition(780, 355);

    final raimunda = ego = new Actor("raimunda", background);
    raimunda.setPosition(330, 355);

    var newspaper:AtlasBitmap;
    oneShot(() -> Res.bgm0.play(true));
    walk(raimunda, 780);
    oneShot(() -> raimunda.y = 405);
    say(rodo, "Did you have lunch?");
    say(rodo, "Are you feeling better?");
    say(raimunda, "Yes, a little.");
    say(raimunda, "Now that everything is\nback to normal.");
    say(rodo, "Indeed.");
    oneShot(() -> {
	Res.sfx_newspaper.play();
	final bg = new h2d.Bitmap(h2d.Tile.fromColor(0x504c4c), background);
	bg.width = scene.width;
	bg.height = scene.height;
	newspaper = new AtlasBitmap("background_end", bg, h2d.Flow.FlowAlign.Middle, h2d.Flow.FlowAlign.Middle);
	newspaper.setPosition(Std.int(scene.width / 2), Std.int(scene.height / 2));
	newspaper.setScale(.1);
      });
    callback((dt) -> {
	newspaper.rotate(20 * dt);
	newspaper.setScale(newspaper.scaleX + dt);
	if (newspaper.scaleX >= 1) {
	  newspaper.setScale(1);
	  newspaper.rotation = 0;
	  pending.shift();
	}
      });
    wait(1);
    say(rodo, "Back to normal.");
    callback((_) -> return);
  }
}