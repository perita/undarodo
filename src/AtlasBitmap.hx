import h2d.Flow;

class AtlasBitmap extends h2d.Bitmap {
  public function new(tileName:String, ?parent:h2d.Object, halign = FlowAlign.Left, valign = FlowAlign.Top) {
    this.halign = halign;
    this.valign = valign;
    this.tileName = tileName;
    super(getTile(), parent);
    Main.ME.onReloadAtlas(reload);
  }

  function getTile() {
    return hxd.Res.tiles.get(tileName, halign, valign);
  }

  function reload() {
    tile = getTile();
  }

  final tileName:String;
  final halign:FlowAlign;
  final valign:FlowAlign;
}