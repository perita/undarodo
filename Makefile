NULL =

PROJECT = undarodo

SHEETS = \
	background \
	cursor \
	microwave \
	pomme \
	raimunda \
	rodo \
	speech \
	t800 \
	throw \
	$(NULL)

TEXTURES = $(patsubst %,res/%.png,$(SHEETS))
ATLAS = res/tiles.atlas

FONT_TEXTURE := res/AmaticSC.png
FONT_FNT := $(patsubst %.png,%.fnt,$(FONT_TEXTURE))

RESOURCES = \
	$(ATLAS) \
	$(FONT_TEXTURE) \
	$(FONT_FNT) \
	$(TEXTURES) \
	$(NULL)

HAXE_SRC := $(wildcard src/*.hx)

HAXE = haxe
HAXEHXML = $(PROJECT).hxml
HAXESERVERPORT = 6000
HAXEJSHXML = $(PROJECT).js.hxml
JSBIN = bin/$(PROJECT).js
HAXEHLHXML = $(PROJECT).hl.hxml
HLBIN = bin/$(PROJECT).hl
CDIR = bin/out
CBIN = $(CDIR)/main.c

ZIP = zip
ZIPFLAGS = -9 -j
ZIPBIN = bin/$(PROJECT).zip

define HAXE_compile
$(HAXE) --connect $(HAXESERVERPORT) $(1) || ( [ $$? -eq 2 ] && $(HAXE) $(1) )
endef

$(HLBIN): $(HAXEHLHXML) $(HAXEHXML) $(HAXE_SRC) | $(RESOURCES)
	$(call HAXE_compile,--debug --dce no --hl $@ $<)

$(CBIN): $(HAXEHLHXML) $(HAXEHXML) $(HAXE_SRC) | $(RESOURCES)
	$(call HAXE_compile,--dce full --hl $@ $<)

$(JSBIN): $(HAXEJSHXML) $(HAXEHXML) $(HAXE_SRC) | $(RESOURCES)
	$(call HAXE_compile,--js $@ $<)

$(ZIPBIN): $(JSBIN) bin/index.html
	$(ZIP) $(ZIPFLAGS) $@ $^

$(ATLAS): $(TEXTURES)
	extractatlas $^ $@

res/%.png: assets/%.svg
	IDS=$$(xml select --text --template --match '/svg:svg/svg:g/svg:*' --value-of '@id' --output ';' $<) && \
		inkscape --export-id-only --export-type=png --export-id="$${IDS::${#IDS}-1}" $< && \
		packtextures assets/$*_*.png $@ && \
		rm assets/$*_*.png

res/%.png res/%.fnt: fonts.json assets/%.ttf
	fontgen $<

all: $(HLBIN) $(JSBIN) $(CBIN) dist

dist: $(ZIPBIN)

run: $(HLBIN)
	hl --hot-reload $<

watch:
	while true; do $(MAKE); inotifywait --recursive --event modify assets src; done

listen:
	haxe --server-listen $(HAXESERVERPORT)

clean:
	rm -fr $(JSBIN) $(HLBIN) $(CDIR) $(ZIPBIN) $(RESOURCES)

.PHONY: all dist run watch listen clean
